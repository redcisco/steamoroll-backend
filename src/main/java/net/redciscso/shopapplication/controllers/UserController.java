package net.redciscso.shopapplication.controllers;


import lombok.RequiredArgsConstructor;
import net.redciscso.shopapplication.domain.User;
import net.redciscso.shopapplication.domain.enums.State;
import net.redciscso.shopapplication.service.UserService;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping(value = "/all")
    public String getAllUsers(){
        return userService.findAll().toString();
    }

    @GetMapping(value = "/info")
    public String getUserInfo(@RequestParam String username){
        return userService.findUserByUsername(username).toString();
    }

    @GetMapping("/delete")
    public void deleteUser(@RequestParam Long id){
        userService.deleteById(id);
    }
}
