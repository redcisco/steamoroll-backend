package net.redciscso.shopapplication.controllers;


import lombok.RequiredArgsConstructor;
import net.redciscso.shopapplication.domain.Product;
import net.redciscso.shopapplication.forms.AddProductForm;
import net.redciscso.shopapplication.forms.SignUpForm;
import net.redciscso.shopapplication.service.ProductService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {
    private final ProductService productService;

    @PostMapping("/addProduct")
    public String addNewProduct(@ModelAttribute AddProductForm addProductForm){
        return "You are added new product: " + productService.addNewProduct(addProductForm).toString();
    }

    @DeleteMapping("/deleteProduct")
    public String deleteProductById(@RequestParam Long id){
        productService.deleteProductById(id);
        return "Succesfull!";
    }

    @GetMapping(value = "/allProducts", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAllProduct(){
        return productService.findAllProduct();
    }
}
