package net.redciscso.shopapplication.controllers;


import lombok.RequiredArgsConstructor;
import net.redciscso.shopapplication.forms.SignUpForm;
import net.redciscso.shopapplication.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class SignUpController {
    private final UserService userService;

    @PostMapping("/signUp")
    public String signUp(@ModelAttribute SignUpForm signUpForm){
        userService.signUp(signUpForm);
        return "Complited!";
    }
}
