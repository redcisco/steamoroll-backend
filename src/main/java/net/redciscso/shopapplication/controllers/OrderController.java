package net.redciscso.shopapplication.controllers;

import lombok.RequiredArgsConstructor;
import net.redciscso.shopapplication.domain.Item;
import net.redciscso.shopapplication.service.OrderService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PostMapping("/new")
    public String createNewOrder(@RequestParam Long userId){
        return "New order :" + orderService.createNewOrder(userId).toString();
    }

    @PostMapping("/item")
    public String addNewItem(@RequestParam Long orderId, Long prodId){
        orderService.addNewItem(orderId, prodId);
        return "New item!";
    }

    @GetMapping("/allItems")
    public String getAllItems(@RequestParam Long orderId){
        return orderService.findAllItemsByOrderId(orderId).toString();
    }
}
