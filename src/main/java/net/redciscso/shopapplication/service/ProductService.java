package net.redciscso.shopapplication.service;

import net.redciscso.shopapplication.domain.Product;
import net.redciscso.shopapplication.forms.AddProductForm;

import java.util.List;

public interface ProductService {
    Product findProductById(Long id);

    Product findProductByTitle(String title);

    List<Product> findAllProductIsExist(Boolean isExist);

    List<Product> findAllProduct();

    Product addNewProduct(AddProductForm form);

    void deleteProductById(Long id);
}
