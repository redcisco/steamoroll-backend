package net.redciscso.shopapplication.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.redciscso.shopapplication.domain.Item;
import net.redciscso.shopapplication.domain.Order;
import net.redciscso.shopapplication.domain.User;
import net.redciscso.shopapplication.repository.ItemRepository;
import net.redciscso.shopapplication.repository.OrderRepository;
import net.redciscso.shopapplication.repository.ProductRepository;
import net.redciscso.shopapplication.repository.UserRepository;
import net.redciscso.shopapplication.service.OrderService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Primary
@Slf4j
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final ItemRepository itemRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;


    @Override
    public Order createNewOrder(Long userId) {
        Order newOrder = new Order();
        newOrder.setPrice(0.0);
        newOrder.setUser(userRepository.findById(userId).get());
        return orderRepository.save(newOrder);
    }

    @Override
    public Order findOrderById(Long id) {
        return orderRepository.findById(id).get();
    }

    @Override
    public Order findOrderByUserOrUserId(User user, Long userId) {
        return orderRepository.findByUserOrUserId(user, userId).get();
    }

    @Override
    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public void deleteOrderById(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public void addNewItem(Long orderId, Long prodId) {
        Item newItem = new Item();
        newItem.setOrder(orderRepository.findById(orderId).get());
        newItem.setProduct(productRepository.findById(prodId).get());
        itemRepository.save(newItem);
    }

    @Override
    public void deleteItemById(Long id) {
        itemRepository.deleteById(id);
    }

    @Override
    public List<Item> findAllItemsByOrderId(Long id) {
        return itemRepository.findAllByOrderId(id);
    }
}
