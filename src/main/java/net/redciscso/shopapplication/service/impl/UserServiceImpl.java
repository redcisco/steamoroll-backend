package net.redciscso.shopapplication.service.impl;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.redciscso.shopapplication.domain.User;
import net.redciscso.shopapplication.domain.enums.State;
import net.redciscso.shopapplication.forms.SignUpForm;
import net.redciscso.shopapplication.repository.UserRepository;
import net.redciscso.shopapplication.service.UserService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username).get();
    }

    @Override
    public User findUserByState(State state) {
        return userRepository.findUserByState(state).get();
    }

    @Override
    public User login(User user) {
        return userRepository.save(user);
    }

    @Override
    public User signUp(SignUpForm signUpForm) {
        User user = new User();
        user.setUsername(signUpForm.getUsername());
        user.setPassword(signUpForm.getPassword());
        user.setEmail(signUpForm.getEmail());
        user.setState(State.ACTIVE);
        user.setAvatarUrl("null");
        System.out.println(user.toString());
        userRepository.save(user);

        return user;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteByUsername(String username) {
        userRepository.deleteByUsername(username);
    }
}
