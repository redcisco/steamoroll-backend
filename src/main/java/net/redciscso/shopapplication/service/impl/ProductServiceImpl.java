package net.redciscso.shopapplication.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.redciscso.shopapplication.domain.Product;
import net.redciscso.shopapplication.forms.AddProductForm;
import net.redciscso.shopapplication.repository.ProductRepository;
import net.redciscso.shopapplication.service.ProductService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public Product findProductById(Long id) {
        return productRepository.findById(id).get();
    }

    @Override
    public Product findProductByTitle(String title) {
        return productRepository.findByTitle(title).get();
    }

    @Override
    public List<Product> findAllProductIsExist(Boolean isExist) {
        return productRepository.findAllByIsExist(isExist);
    }

    @Override
    public List<Product> findAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product addNewProduct(AddProductForm form) {
        Product newProduct = new Product();
        newProduct.setTitle(form.getTitle());
        newProduct.setPrice(form.getPrice());
        newProduct.setPhotoUrl(form.getPhotoUrl());
        newProduct.setCount(form.getCount());
        newProduct.setIsExist(form.getIsExist());
        System.out.println(newProduct.toString());
        return productRepository.save(newProduct);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }
}
