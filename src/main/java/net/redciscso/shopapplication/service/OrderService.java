package net.redciscso.shopapplication.service;

import net.redciscso.shopapplication.domain.Item;
import net.redciscso.shopapplication.domain.Order;
import net.redciscso.shopapplication.domain.User;

import java.util.List;

public interface OrderService {

    Order createNewOrder(Long userId);

    Order findOrderById(Long id);

    Order findOrderByUserOrUserId(User user, Long userId);

    List<Order> findAllOrders();

    void deleteOrderById(Long id);

    void addNewItem(Long orderId, Long prodId);

    void deleteItemById(Long id);

    List<Item> findAllItemsByOrderId(Long id);

}
