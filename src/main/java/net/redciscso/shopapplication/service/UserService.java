package net.redciscso.shopapplication.service;


import net.redciscso.shopapplication.domain.User;
import net.redciscso.shopapplication.domain.enums.State;
import net.redciscso.shopapplication.forms.SignUpForm;

import java.util.List;


public interface UserService {

    User findUserById(Long id);

    User findUserByUsername(String username);

    User findUserByState(State state);

    User login(User user);

    User signUp(SignUpForm signUpForm);

    List<User> findAll();

    void deleteById(Long id);

    void deleteByUsername(String username);


}
