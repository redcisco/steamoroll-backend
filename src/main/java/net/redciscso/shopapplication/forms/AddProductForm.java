package net.redciscso.shopapplication.forms;


import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AddProductForm {

    private String title;
    private Double price;
    private String photoUrl;
    private Integer count;
    private Boolean isExist;

}
