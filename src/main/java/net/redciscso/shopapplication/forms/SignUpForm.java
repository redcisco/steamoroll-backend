package net.redciscso.shopapplication.forms;


import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class SignUpForm {

    private String username;
    private String password;
    private String email;
}
