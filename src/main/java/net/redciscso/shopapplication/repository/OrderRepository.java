package net.redciscso.shopapplication.repository;

import net.redciscso.shopapplication.domain.Order;
import net.redciscso.shopapplication.domain.Product;
import net.redciscso.shopapplication.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> findByUserOrUserId(User user, Long userId);



}
