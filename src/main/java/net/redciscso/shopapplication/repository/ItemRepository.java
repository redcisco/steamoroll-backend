package net.redciscso.shopapplication.repository;


import net.redciscso.shopapplication.domain.Item;
import net.redciscso.shopapplication.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findAllByOrderId(Long orderId);
}
