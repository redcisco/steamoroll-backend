package net.redciscso.shopapplication.repository;

import net.redciscso.shopapplication.domain.User;
import net.redciscso.shopapplication.domain.enums.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByUsername(String username);

    Optional<User> findUserByState(State state);

    void deleteByUsername(String username);
}
