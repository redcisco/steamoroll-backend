package net.redciscso.shopapplication.repository;

import net.redciscso.shopapplication.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findByTitle(String title);

    List<Product> findAllByIsExist(Boolean isExist);
}
