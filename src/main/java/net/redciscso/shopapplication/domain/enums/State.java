package net.redciscso.shopapplication.domain.enums;

public enum State {
    ACTIVE,
    BANNED
}
