package net.redciscso.shopapplication.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "prod_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "prod_title")
    private String title;

    @Column(name = "prod_price")
    private Double price;

    @Column(name = "prod_photo_url")
    private String photoUrl;

    @Column(name = "prod_count")
    private Integer count;

    @Column(name = "prod_exist")
    private Boolean isExist;
}
