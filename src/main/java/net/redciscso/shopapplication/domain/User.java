package net.redciscso.shopapplication.domain;

import lombok.*;
import net.redciscso.shopapplication.domain.enums.State;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USERS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "AVATARURL")
    private String avatarUrl;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Order> orders;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", state=" + state +
                ", avatarUrl='" + avatarUrl + '\'' +
                '}';
    }
}
