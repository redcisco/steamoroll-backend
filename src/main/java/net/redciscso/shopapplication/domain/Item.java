package net.redciscso.shopapplication.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "item")
public class Item {

    @Id
    @Column(name = "item_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "item_order_id", nullable = false)
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "item_prod_id", nullable = false)
    private Product product;

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", orderId=" + order.getId() +
                ", prodId=" + product.getId() +
                '}';
    }
}
